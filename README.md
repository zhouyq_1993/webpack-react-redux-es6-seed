﻿# webpack-react-redux-es6-boilerplate !

使用 webpack + react + redux + es6 的组件化前端项目 seed

因为最近在工作中尝试了 [webpack](https://github.com/webpack/webpack)、[react](https://github.com/facebook/react)、[redux](https://github.com/reactjs/redux)、[es6](http://babeljs.io/docs/learn-es2015/) 技术栈，所以总结出了一套 [boilerplate](https://github.com/xiaoyann/webpack-react-redux-es6-boilerplate)，以便下次做项目时可以快速开始，并进行持续优化。

> 开发时的环境

- [x] Node v6.3.0
- [x] NPM v3.10.3

> 快速开始

```
$ npm install
$ npm start
```

> windows 兼容性问题

* 已修复 2016年09月05日15:03:27
* 因为使用了 node-sass ，需要先安装 [Microsoft Windows SDK for Windows 7 and .NET Framework 4
](https://www.microsoft.com/en-us/download/details.aspx?id=8279)
